

let socket = new WebSocket("wss://radio.freelancingpeter.eu/api/live/nowplaying/websocket");

socket.onopen = function(e) {
  socket.send(JSON.stringify({
    "subs": {
      "station:ti_pade_lancut_radio": {"recover": true}
    }
  }));
};

var nowplaying = {};
var currentTime = 0;

function handleSseData(ssePayload, useTime = true) {
  const jsonData = ssePayload.data;

  if (useTime && 'current_time' in jsonData) {
    currentTime = jsonData.current_time;
  }

  nowplaying = jsonData.np;
  console.log(nowplaying.now_playing.song.title);

  let currentSong = document.getElementById('currentSong');
  currentSong.innerHTML = `${nowplaying.now_playing.song.title} - ${nowplaying.now_playing.song.artist}`;
  currentSong.disabled = true;

  let songList = document.getElementById('lastSongsList');

  let songListHtml = '<ul>';
  nowplaying.song_history.forEach(song => {

    songListHtml += `<li>${song.song.title} - ${song.song.artist}</li>`;
  });
  songListHtml += '</ul>';
  songList.innerHTML = songListHtml;



}

async function fullyLoaded(){
socket.onmessage = function(e) {
  const jsonData = JSON.parse(e.data);

  if ('connect' in jsonData) {
    const connectData = jsonData.connect;

    if ('data' in connectData) {
      // Legacy SSE data
      connectData.data.forEach(
        (initialRow) => handleSseData(initialRow)
      );
    } else {
      // New Centrifugo time format
      if ('time' in connectData) {
        currentTime = Math.floor(connectData.time / 1000);
      }

      // New Centrifugo cached NowPlaying initial push.
      for (const subName in connectData.subs) {
        const sub = connectData.subs[subName];
        if ('publications' in sub && sub.publications.length > 0) {
          sub.publications.forEach((initialRow) => handleSseData(initialRow, false));
        }
      }
    }
  } else if ('pub' in jsonData) {
    handleSseData(jsonData.pub);
  }
};

}



async function playPlayer() {
    
    let playerId = document.getElementById('player');
    document.getElementById("play").disabled = true;
    playerId.src = "https://radio.freelancingpeter.eu/listen/ti_pade_lancut_radio/radio.flac"
    playerId.setAttribute("type","audio/flac");
    try {
      await  playerId.play();
    } catch (err) {
      // If it gets back and was rejected, here we will console log the error to see what exactly failed
      // In your case, the IP address you used failed to load the music. 
        console.log(err);
    }
   
}
async function stopPlayer() {
    let playerId = document.getElementById('player');
    document.getElementById("play").disabled = false;
    playerId.load();
   
}
async function pausePlayer() {
    let playerId = document.getElementById('player');
    document.getElementById("play").disabled = false;
    playerId.pause();
}


document.getElementById("play").addEventListener('click', () => {
    playPlayer();
    fullyLoaded();
  });
  document.getElementById("pause").addEventListener('click', () => {
    pausePlayer();
  });
  document.getElementById("stop").addEventListener('click', () => {
    stopPlayer();
  });


  const audioPlayer = document.getElementById('player');
  const volumeSlider = document.getElementById('volume-slider');

  volumeSlider.addEventListener('input', () => {
    audioPlayer.volume = volumeSlider.value;
  });
